from typing import List

from fastapi import APIRouter, Depends
from sqlalchemy.orm.session import Session

from listerical_api.db.database import get_session
from .repository import list_dishes
from .schemas import Dish

router = APIRouter()


@router.get("", response_model=List[Dish])
def get_dishes(session: Session = Depends(get_session)) -> List[Dish]:
    return list_dishes(session)
