from typing import List

from sqlalchemy.orm.session import Session

from .models import Dish as DishORM
from .schemas import Dish


def list_dishes(session: Session) -> List[Dish]:
    dishes = session.query(DishORM).all()
    return [Dish.from_orm(dish) for dish in dishes]
