import enum

from sqlalchemy import Column, Enum, Integer, String

from listerical_api.db.database import Base


class FoodType(str, enum.Enum):
    dairy = "dairy"
    neutral = "neutral"
    meat = "meat"


class Dish(Base):
    __tablename__ = "dishes"

    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False, unique=True)
    description = Column(String(200))
    image = Column(String(200), nullable=False)
    calories = Column(Integer, nullable=False)
    food_type = Column(Enum(FoodType), nullable=False)
