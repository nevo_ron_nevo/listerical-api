from typing import Iterator

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.session import Session, close_all_sessions
from sqlalchemy.pool import StaticPool

Base = declarative_base()
create_session = sessionmaker(autocommit=False, autoflush=False)


def connect(db_uri: str, max_connections: int = 100) -> None:
    if db_uri.startswith("sqlite://"):
        engine = create_engine(db_uri, poolclass=StaticPool)
    else:
        engine = create_engine(db_uri, pool_size=max_connections)
    create_session.configure(bind=engine)
    Base.metadata.create_all(engine)


def disconnect() -> None:
    close_all_sessions()


def get_session() -> Iterator[Session]:
    session = create_session()
    yield session
    session.close()
