from loguru import logger

from listerical_api.core.config import settings

from .database import connect, disconnect


def db_startup() -> None:
    if not settings.db_uri:
        raise ValueError("Database URI is not set")
    logger.info("Connecting to {}", settings.db_uri)
    connect(settings.db_uri)
    logger.info("Connection established")


def db_shutdown() -> None:
    logger.info("Closing connection to {}", settings.db_uri)
    disconnect()
    logger.info("Connection closed")
