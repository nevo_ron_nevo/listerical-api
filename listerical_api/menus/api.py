from datetime import date

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm.session import Session

from listerical_api.db.database import get_session
from .repository import get_menu_by_date
from .schemas import Menu

router = APIRouter()


@router.get("/{menu_date}", response_model=Menu)
def get_menu(menu_date: date, session: Session = Depends(get_session)) -> Menu:
    menu = get_menu_by_date(session, menu_date)

    if not menu:
        raise HTTPException(status_code=404, detail="Menu not found")

    return menu
