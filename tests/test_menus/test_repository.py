import pytest

from datetime import date
from unittest.mock import MagicMock

from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.orm.session import Session

from listerical_api.menus.models import Dish, Meal, Menu
from listerical_api.menus.repository import get_menu_by_date
from listerical_api.menus.schemas import Menu as MenuSchema


def test_get_menu_by_date_duplicate_fails() -> None:
    session = MagicMock()
    session.query.side_effect = MultipleResultsFound()

    assert get_menu_by_date(session, date.today()) is None


@pytest.mark.integration_test
def test_get_menu_by_date_not_found(db_session: Session) -> None:
    test_date = "2020-10-25"

    assert get_menu_by_date(db_session, date.fromisoformat(test_date)) is None


@pytest.mark.integration_test
def test_get_menu_by_date_success(db_session: Session) -> None:
    test_menu = Menu(
        date=date.fromisoformat("2020-10-25"),
        meals=[
            Meal(
                name="Breakfast",
                opening_hours="8:00 - 10:00",
                dishes=[
                    Dish(
                        name="Pasta",
                        description="Yummy.",
                        image="https://somewhere.on.the/web.png",
                        calories=400,
                        food_type="dairy",
                    )
                ],
            )
        ],
    )
    db_session.add(test_menu)
    db_session.commit()

    assert get_menu_by_date(db_session, test_menu.date) == MenuSchema.from_orm(test_menu)
