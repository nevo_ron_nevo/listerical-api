from fastapi.testclient import TestClient
from sqlalchemy.orm.session import Session

from listerical_api.health.models import HealthCheck


def test_health_ok(client: TestClient) -> None:
    assert client.get("/api/health").json() == {"status": "ok"}


def test_health_works_twice(client: TestClient, db_session: Session) -> None:
    db_session.query(HealthCheck).delete()

    assert client.get("/api/health").json() == {"status": "ok"}
    assert db_session.query(HealthCheck).count() == 1

    assert client.get("/api/health").json() == {"status": "ok"}
    assert db_session.query(HealthCheck).count() == 1
