from _pytest.monkeypatch import MonkeyPatch
from pytest_mock import MockFixture

from listerical_api.core._config import parse_config


def test_config_db_uri_set(monkeypatch: MonkeyPatch, mocker: MockFixture) -> None:
    sys_exit = mocker.patch("sys.exit")
    monkeypatch.setenv("LISTERICAL_DB_URI", "mysql://localhost:3306")
    parse_config()
    sys_exit.assert_not_called()


def test_config_missing_db_uri_exits(monkeypatch: MonkeyPatch, mocker: MockFixture) -> None:
    sys_exit = mocker.patch("sys.exit")
    monkeypatch.delenv("LISTERICAL_DB_URI", raising=False)
    parse_config()
    sys_exit.assert_called_once_with(1)
