from fastapi.testclient import TestClient
from pytest_mock import MockerFixture

from listerical_api.menus.schemas import Dish


def test_get_menu_success(client: TestClient, mocker: MockerFixture) -> None:
    expected = [
        Dish(
            name="Some dish I guess",
            description="You should definitely eat it!",
            image="https://img.delicious.com.au/WqbvXLhs/del/2016/06/more-the-merrier-31380-2.jpg",
            calories=100,
            food_type="dairy",
        )
    ]

    mocker.patch("listerical_api.dishes.api.list_dishes").return_value = expected
    response = client.get("/api/dishes")

    assert response.status_code == 200
    assert [Dish(**dish) for dish in response.json()] == expected
