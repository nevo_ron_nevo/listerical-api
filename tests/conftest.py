import os

import pytest
import requests

from typing import Iterator

from fastapi.testclient import TestClient
from sqlalchemy.orm.session import Session
from testcontainers.mysql import MariaDbContainer

TEST_DB_URI = "sqlite:///?check_same_thread=False"
os.environ["LISTERICAL_DB_URI"] = TEST_DB_URI

from listerical_api.db.database import connect, disconnect, get_session  # noqa: E402
from listerical_api.main import app  # noqa: E402


@pytest.fixture(scope="function")
def client() -> Iterator[requests.Session]:
    with TestClient(app) as client:
        yield client


@pytest.fixture(scope="session")
def mariadb_uri() -> Iterator[str]:
    with MariaDbContainer(image="mariadb:10.5.6") as database:
        yield database.get_connection_url()


@pytest.fixture(scope="function")
def db_session() -> Iterator[Session]:
    connect(TEST_DB_URI)
    yield from get_session()
    disconnect()
